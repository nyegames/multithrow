﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBlobs : MonoBehaviour
{
    private Image[] _Blobs;

    private Single _Time;

    public Single AnimSpeed = 1.9f;

    public Boolean Animating { get; set; } = false;

    // Start is called before the first frame update
    void Start()
    {
        _Blobs = GetComponentsInChildren<Image>().OrderBy(s => Int32.Parse(s.gameObject.name.Replace("LoadingImage", ""))).ToArray();
        for (int i = _Blobs.Length - 1; i >= 0; i--)
        {
            Image image = _Blobs[i];
            float f = i % 2 != 0 ? 0.5f : 0;
            image.transform.localScale = new Vector3(f, f, 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Animating) return;
        _Time += Time.deltaTime;

        for (int i = 0; i < _Blobs.Length; i++)
        {
            Image image = _Blobs[i];
            float s = image.transform.localScale.x;

            float t = s + (Time.deltaTime * (Math.Abs(AnimSpeed)));
            if (t > 1)
            {
                t = 0f;
                float h = image.rectTransform.sizeDelta.y;
                float localPositionY = image.transform.localPosition.y;
                bool b = localPositionY > 0;

                //Middle blob moves all the way up
                if (i == _Blobs.Length / 2)
                {
                    image.transform.localPosition = b ? new Vector3(image.transform.localPosition.x, -h * 2f) : new Vector3(image.transform.localPosition.x, h * 2f);

                    transform.localRotation = Quaternion.AngleAxis(b ? 90 : 0, Vector3.forward);

                }
                //Every other blob moves either side of the center
                else if (i % 2 != 0)
                {
                    image.transform.localPosition = b ? new Vector3(image.transform.localPosition.x, -h) : new Vector3(image.transform.localPosition.x, h);
                }
            }

            image.transform.localScale = new Vector3(t, t, 1);
        }
    }
}
