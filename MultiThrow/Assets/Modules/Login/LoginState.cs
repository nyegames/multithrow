﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginState : MonoBehaviour
{
    private readonly System.Object _InputService;

    public Button LoginButton;

    [SerializeField] private PlayerProfile _PlayerProfile;
    [SerializeField] private GameDatabase _Database;

    private PlayerProfileData _PlayerData;

    public void Login()
    {
        if (LoginButton != null)
        {
            Text loginText = LoginButton.GetComponentInChildren<Text>();
            if (loginText != null)
            {
                loginText.text = "...";
            }
            LoginButton.enabled = false;
        }

        StartCoroutine(LoginSequence());
    }

    private IEnumerator LoginSequence()
    {
        string dataPlayerID = _PlayerProfile.ID;
        Guid id = Guid.Parse(dataPlayerID);

        string content = JsonUtility.ToJson(new PlayerProfileData { player_id = id.ToString() });
        PostDatabaseRequestAsync<PlayerProfileData> loginOp = _Database.PostRequest<PlayerProfileData>(_Database.LoginQuery, content);

        yield return loginOp;

        SceneManager.LoadScene(1);
    }
}