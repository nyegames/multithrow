﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MatchmakeSimuationState : MonoBehaviour
{
    public GameObject ScrollContent;

    public GameObject SimulationPrefab;

    /// <summary>How many ranks the simulations you play can be below you</summary>
    public Int32 MinRankOffset = 2;

    /// <summary>How many ranks the simulations can be above you</summary>
    public Int32 MaxRankOffset = 1;

    [SerializeField] private PlayerProfile _PlayerProfile;
    [SerializeField] private GameDatabase _Database;

    public IEnumerator Start()
    {
        if (string.IsNullOrEmpty(_PlayerProfile.Simulation?.simulation_id))
        {
            GetArrayDatabaseRequestAsync<SimulationInformation> simLoad = _Database.GetArrayReqest<SimulationInformation>(_Database.GetAllSimsQuery);

            yield return simLoad;

            SimulationInformation[] sims = simLoad.Result;

            //If there is no simulations
            if (!(sims?.Any() ?? false))
            {
                //Null so one gets auto generated for you
                _PlayerProfile.Simulation = null;
                SceneManager.LoadScene(2);
            }
            else
            {
                //Sort the simulations into the Rank bracket you are in
                sims = sims.Where(s => s.owner_player.rank >= _PlayerProfile.Rank - MinRankOffset).ToArray();
                sims = sims.Where(s => s.owner_player.rank <= _PlayerProfile.Rank + MaxRankOffset).ToArray();

                if (!sims.Any())
                {
                    //Null so one gets auto generated for you
                    _PlayerProfile.Simulation = null;
                    SceneManager.LoadScene(2);
                }
                else
                {
                    SimulationsGrabbed(sims.ToList());
                }
            }

        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }

    private void SimulationsGrabbed(List<SimulationInformation> sims)
    {
        sims = sims.OrderBy(s => s.owner_player.rank).ToList();

        float maxHeight = 0f;

        for (int i = 0; i < sims.Count; i++)
        {
            SimulationInformation simInfo = sims[i];
            List<GameObject> children = ScrollContent.transform.GetComponentsInChildren<SimulationDetails>()
                .Select(s => s.gameObject).ToList();

            RectTransform first = children.FirstOrDefault()?.GetComponent<RectTransform>() ?? null;
            float height = first?.sizeDelta.y ?? 0;
            maxHeight += height;
            if (i == sims.Count - 1)
            {
                maxHeight += height;
            }

            Vector3 position = new Vector3(0, -height * children.Count);

            GameObject simButton = Instantiate(SimulationPrefab);
            simButton.transform.parent = ScrollContent.transform;
            simButton.transform.localPosition = position;
            simButton.transform.localScale = new Vector3(1, 1, 1);

            string simId = simInfo.simulation_id;
            string shortID = simId.Take(5).ToArray().ArrayToString();
            simButton.GetComponentInChildren<Text>().text = $"Rank: {simInfo.owner_player.rank}: [{shortID}]";

            SimulationDetails simButtonDetails = simButton.GetComponentInChildren<SimulationDetails>();
            simButtonDetails.Simulation = simInfo;

            simButton.GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                OnSimulationChosen(simInfo);
            });
        }

        Vector2 sizeDelta = ScrollContent.GetComponent<RectTransform>().sizeDelta;
        ScrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(sizeDelta.x, maxHeight);

        if (sims.Count == 0)
        {
            OnSimulationChosen(null);
        }
    }

    public void OnSimulationChosen(SimulationInformation sim)
    {
        _PlayerProfile.Simulation = sim;
        SceneManager.LoadScene(2);
    }
}