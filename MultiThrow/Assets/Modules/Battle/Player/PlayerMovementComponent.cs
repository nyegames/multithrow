﻿using System;
using UnityEngine;

class PlayerMovementComponent : MonoBehaviour
{
    private Single _PlayerSpeed = 3f;

    [SerializeField] private PlayerDefaults _PlayerDefaults;
    [SerializeField] private PlayArea _PlayArea;

    private Int32 _Direction;

    public void Start()
    {
        _Direction = _PlayerDefaults.StartingDirection;
    }

    public void Update()
    {
        transform.localPosition += new Vector3(_Direction * _PlayerSpeed * Time.deltaTime * Time.timeScale, 0f);

        int width = 1;
        float left = transform.position.x - width;
        float right = transform.position.x + width;

        Rect area = _PlayArea.Area;

        //If you are moving right and are now outside the bounds, move left
        if (_Direction > 0 && right > area.x + area.width)
        {
            _Direction = -1;
        }
        else if (_Direction < 0 && left < area.x)
        {
            _Direction = 1;
        }
    }
}

