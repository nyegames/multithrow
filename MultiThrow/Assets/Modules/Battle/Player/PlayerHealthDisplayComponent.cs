﻿using System;
using System.Linq;
using UnityEngine;

public class PlayerHealthDisplayComponent : MonoBehaviour
{
    [SerializeField] private SpriteRenderer[] _Health;

    // Start is called before the first frame update
    void Start()
    {
        _Health = GetComponentsInChildren<SpriteRenderer>().OrderBy(s => Int32.Parse(s.gameObject.name.Replace("Health", ""))).ToArray();
    }

    public void OnPlayerHealthChanged(Int32 oldHp, Int32 newHp, String tag)
    {
        if (gameObject.tag != tag) return;

        for (int index = 0; index < _Health.Length; index++)
        {
            SpriteRenderer spriteRenderer = _Health[index];
            spriteRenderer.enabled = index < newHp;
        }
    }
}
