﻿public class OnClickPowerTriggerComponent : PowerTriggerComponent
{
    #region Overrides of PowerBehaviourComponent

    protected override void OnStart()
    {
        base.OnStart();
        InputEnabled = true;
    }

    protected override void OnPowerClicked()
    {
        base.OnPowerClicked();

        DisableIcon();

        ConsumePower();
        TriggerPower();
        Destroy(gameObject);
    }

    #endregion
}
