﻿using Assets.Scripts.Data.Events.Powers;
using System;
using System.Linq;
using UnityEngine;

//TODO - Remove this abstract class and instead replace it with SerialisedObject Input System
public abstract class PowerTriggerComponent : MonoBehaviour, IInputListener
{
    protected BoxCollider2D _Collider2D;

    private Boolean _Consumed;

    private Boolean _Triggered;

    [SerializeField] private PowerID _PowerID;

    [SerializeField] private InputData _InputData;

    public PowerTriggeredEvent PowerTriggered;

    public PowerConsumedEvent PowerConsumedEvent;

    public void Start()
    {
        _InputData.Register(this);
        _Collider2D = GetComponent<BoxCollider2D>();
        OnStart();
    }

    public void OnDestroy()
    {
        _InputData.UnRegister(this);
        OnDestroyed();
    }

    protected virtual void OnStart()
    {

    }

    protected virtual void OnPowerClicked()
    {

    }

    /// <summary>By default this will disable all <see cref="SpriteRenderer"/> components inside the <see cref="GameObject"/> this script is located on, and disables all
    /// <see cref="BoxCollider2D"/> on this <see cref="GameObject"/>
    /// </summary>
    protected virtual void DisableIcon()
    {
        _Collider2D.enabled = false;
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();
        if (sprite != null)
        {
            sprite.enabled = false;
        }

        InputEnabled = false;
    }

    protected virtual void OnDestroyed()
    {

    }

    /// <summary>Takes this power out of the pool of powers and allows more to be spawned</summary>
    protected void ConsumePower()
    {
        if (_Consumed) return;
        _Consumed = true;

        PowerConsumedEvent?.Raise(Constants.Tag.PLAYER, _PowerID, transform.position);
    }

    protected virtual String GetPowerExtraData()
    {
        return "";
    }

    protected void TriggerPower()
    {
        if (_Triggered) return;
        _Triggered = true;

        string powerExtraData = GetPowerExtraData();
        PowerTriggered?.Raise(tag, _PowerID, transform.position, powerExtraData);
    }

    public void Update()
    {
        OnUpdated(Time.deltaTime * Time.timeScale);
    }

    protected virtual void OnUpdated(Single delta)
    {

    }

    #region Implementation of IInputListener

    public bool InputEnabled { get; set; }

    public int Order { get; private set; }

    public bool OnPressed(Vector2 mousePositon, out bool consume)
    {
        RaycastHit2D[] hit = Physics2D.RaycastAll(mousePositon, Vector2.zero);

        if (Constants.Tag.PLAYER == tag && hit.Any(s => s.collider == _Collider2D))
        {
            OnPowerClicked();

            return consume = true;
        }
        else
        {
            return consume = false;
        }
    }

    #endregion
}