﻿using System;
using System.Linq;
using UnityEngine;
using Color = UnityEngine.Color;
using Random = UnityEngine.Random;

public class PowerSystem : MonoBehaviour
{
    [SerializeField] private Single _MinSpawnCoolDown = 1.2f;

    [SerializeField] private Single _PowerSpawnCooldown = 4;

    /// <summary></summary>
    [SerializeField] private Single _IncreaseMultiplierPerPower = 0.16f;

    [SerializeField] private Int32 _MyPowersActive;

    /// <summary>How many powers need to be consumed before you can continue to spawn more powers</summary>
    [SerializeField] private Int32 _MaximumActivePowers = 3;

    [SerializeField] private Single _ElapsedTime = 4;

    [SerializeField] private float _HorizontalTrimPercent = 0.5f;
    [SerializeField] private float _VerticalTrimPercent = 0.33f;

    [SerializeField] private PowerLibrary _PowerLibrary;

    [SerializeField] private PlayArea _PlayArea;

    private Rect? GetSpawnArea()
    {
        Rect? playAreaRect = _PlayArea?.Area;
        if (playAreaRect == null) return null;
        return RectHelper.ReduceRect(playAreaRect.Value, _HorizontalTrimPercent, _VerticalTrimPercent);
    }

    public void OnDrawGizmos()
    {
        Rect? playAreaRect = GetSpawnArea();
        if (playAreaRect == null) return;
        GizmoHelper.DrawRectGizmo(playAreaRect.Value, Color.blue);
    }

    public void Update()
    {
        if (_MyPowersActive >= _MaximumActivePowers)
        {
            return;
        }

        _ElapsedTime -= Time.deltaTime * Time.timeScale;
        if (_ElapsedTime > 0) return;
        _MyPowersActive++;
        _PowerSpawnCooldown -= _PowerSpawnCooldown * _IncreaseMultiplierPerPower;
        _PowerSpawnCooldown = Math.Max(_MinSpawnCoolDown, _PowerSpawnCooldown);
        _ElapsedTime = _PowerSpawnCooldown;

        Rect spawnArea = GetSpawnArea().GetValueOrDefault();

        float x = spawnArea.x + Random.Range(0, spawnArea.width);
        float y = spawnArea.y - Random.Range(0, spawnArea.height);
        Vector2 end = new Vector2(x, y);

        PowerID randomPower = _PowerLibrary.GetAll().RandomItem();

        PowerSpawnData data = new PowerSpawnData
        {
            PlayerTag = Constants.Tag.PLAYER,
            PowerID = randomPower,
            Position = end
        };

        _PowerLibrary.CreateTrigger(data);
    }

    public void OnPowerConsumed(String powerTag, PowerID powerID, Vector2 spawn)
    {
        if (Constants.Tag.PLAYER == powerTag)
        {
            _MyPowersActive--;
        }
        else if (Constants.Tag.SIMULATION == powerTag)
        {
            PowerComponent[] activePowers = FindObjectsOfType<PowerComponent>();
            PowerComponent toRemove = activePowers.FirstOrDefault(s => s.tag == Constants.Tag.SIMULATION && s.ID == powerID);
            if (toRemove != null)
            {
                Destroy(toRemove.gameObject);
            }
        }
    }

    public void OnPowerTriggered(String powerTag, PowerID powerID, Vector2 spawn, String powerData)
    {
        powerID?.TriggerPower(powerTag, spawn, powerData);
    }


    public void OnPowerCreated(PowerCreatedEventData eventData)
    {
        if (eventData.Tag != Constants.Tag.SIMULATION) return;

        PowerComponent[] allObjs = FindObjectsOfType<PowerComponent>();
        GameObject simulatedPower = allObjs.FirstOrDefault(s => s.gameObject.GetInstanceID() == eventData.InstanceID)?.gameObject;

        if (simulatedPower == null) return;

        //Locate the "Icon" child, and find all the sprites. Fade them out a little for all simulated powers
        SpriteRenderer[] spriteRenderers = simulatedPower.transform.Find("Icon")?.GetComponents<SpriteRenderer>() ??
                                           new SpriteRenderer[0];
        foreach (SpriteRenderer spriteRenderer in spriteRenderers)
        {
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b,
                0.1f);
        }
    }
}

