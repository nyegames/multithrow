﻿using Assets.Scripts.Data.Abilities;
using System;
using UnityEngine;

public class OnNextAbilityPowerTriggerComponent : PowerTriggerComponent
{
    private Boolean _WaitingForCreation;

    private AbilityID _AbilityID;
    private Vector2 _TargetDirection;

    protected override void OnStart()
    {
        base.OnStart();
        InputEnabled = true;
    }

    #region Overrides of PowerBehaviourComponent

    protected override void OnPowerClicked()
    {
        base.OnPowerClicked();

        InputEnabled = false;
        _WaitingForCreation = true;

        //Consume it immediately, this allows for stacking multi shots.
        ConsumePower();

        DisableIcon();
    }

    #region Overrides of PowerTriggerComponent

    protected override string GetPowerExtraData()
    {
        AbilityCreationData abilityCreated = new AbilityCreationData
        {
            AbilityID = _AbilityID,
            TargetDirection = _TargetDirection.ToJsonVector2()
        };
        return JsonUtility.ToJson(abilityCreated);
    }

    #endregion

    public void AbilityCreated(String abilityTag, AbilityID abiltiyid, Vector2 spawn, Vector2 direction)
    {
        if (_WaitingForCreation && tag == abilityTag)
        {
            _WaitingForCreation = false;

            _AbilityID = abiltiyid;
            _TargetDirection = direction;

            AbilityCreationEventListener abilityCreationEventListener = GetComponent<AbilityCreationEventListener>();
            abilityCreationEventListener.Event.UnregisterListener(abilityCreationEventListener);

            TriggerPower();

            Destroy(gameObject);
        }
    }

    #endregion
}