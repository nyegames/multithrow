﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Image = UnityEngine.UI.Image;

public class AbilityPreview : MonoBehaviour
{
    [SerializeField] private Image[] _Previews;

    [SerializeField] private AbilityQueueComponent _AbilityQueue;

    [SerializeField] private AbilityColour[] AbilityColours;

    [SerializeField] private AbilityLibrary AbilityLibrary;

    [Serializable]
    public class AbilityColour
    {
        public ElementType Element;
        public Color Color;
    }

    // Start is called before the first frame update
    void Start()
    {
        _Previews = GetComponentsInChildren<Image>().OrderBy(s => Int32.Parse(s.gameObject.name.Replace("Preview", ""))).ToArray();

        _AbilityQueue = FindObjectOfType<AbilityQueueComponent>();

        StartCoroutine(UpdateWhenAvailable());
    }

    private IEnumerator UpdateWhenAvailable()
    {
        yield return new WaitUntil(() =>
        {
            AbilityID abilityID = _AbilityQueue.GetNext3Abilities().FirstOrDefault();
            if (abilityID == null) return false;
            ElementType element = AbilityLibrary?.GetElement(abilityID);
            return element != null;
        });
        UpdatePreviews();
    }

    public void UpdatePreviews()
    {
        AbilityID[] abilities = _AbilityQueue.GetNext3Abilities();
        for (int i = 0; i < abilities.Length; i++)
        {
            AbilityID ability = abilities[i];
            ElementType element = AbilityLibrary.GetElement(ability);
            _Previews[i].color = AbilityColours.FirstOrDefault(s => s.Element == element)?.Color ?? Color.black;
        }
    }
}
