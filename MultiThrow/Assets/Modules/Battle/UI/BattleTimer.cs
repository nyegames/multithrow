﻿using UnityEngine;
using UnityEngine.UI;

public class BattleTimer : MonoBehaviour
{
    [SerializeField] private Slider _Slider;

    [SerializeField] private GameRoundDataModel _DataModel;

    // Start is called before the first frame update
    void Start()
    {
        if (_Slider == null) _Slider = GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        float seconds = _DataModel.GetSecondsRemaining();
        float t = MathHelper.NormaliseRange(0, 30, seconds);

        _Slider.value = t;
    }
}
