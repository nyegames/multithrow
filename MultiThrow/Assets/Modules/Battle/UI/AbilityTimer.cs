﻿using Glide;
using System;
using UnityEngine;
using UnityEngine.UI;

public class AbilityTimer : MonoBehaviour
{
    private AbilityQueueComponent _AbilityQueue;

    private Tweener _Tweener = new Tweener();

    [SerializeField] private Slider _Slider;
    [SerializeField] private Image _Image;

    // Start is called before the first frame update
    void Start()
    {
        if (_Slider == null) _Slider = GetComponent<Slider>();
        if (_Image == null) _Image = GetComponentInChildren<Image>();

        _AbilityQueue = FindObjectOfType<AbilityQueueComponent>();
    }

    private void OnAbilityRecharging(AbilityID id, float t)
    {
        if (t <= 0) return;
        _Slider.value = 1 - t;
    }

    private void OnAbilityReady(AbilityID id)
    {
        _Slider.value = 1;

        _Image.color = Color.white;
        _Image.transform.localScale = new Vector3(1, 1, 1f);

        Color col = _Image.color;
        Tween twn = _Tweener.Tween(_Image, new { color = new Color(col.r, col.g, col.b, 0.3f) }, 0.3f);
        twn.Repeat(-1).Reflect();
    }

    // Update is called once per frame
    void Update()
    {
        _Tweener?.Update(Time.deltaTime * Time.timeScale);
    }

    public void OnDestroy()
    {
        _Tweener = null;
    }

    public void AbilityCreated(String abilityTag, AbilityID abiltiyid, Vector2 spawn, Vector2 direction)
    {
        if (abilityTag != Constants.Tag.PLAYER) return;
        _Tweener.Tween(_Image, new { color = Color.white }, 0f);
    }

    public void AbilityRechargeUpdate(AbilityID id, Single t)
    {
        OnAbilityRecharging(id, t);
        if (t >= 1)
        {
            OnAbilityReady(id);
        }
    }
}