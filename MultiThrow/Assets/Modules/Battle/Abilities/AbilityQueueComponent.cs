﻿using System;
using System.Linq;
using UnityEngine;

/// <summary>
/// Stores a fixed amount of predetermined available Ability ID's which will become available over time.
/// The player can query this component for the latest 3 abilities that will become available.
/// The player can then consume an ability to trigger it.
/// </summary>
public class AbilityQueueComponent : MonoBehaviour, IInputListener
{
    [SerializeField] private AbilityID[] _Abilities;

    [SerializeField] private Single _TimeRemaining;

    [SerializeField] private Boolean _AbilityReady;

    [SerializeField] private AbilityQueueData _AbilityQueueData;

    [SerializeField] private AbilityLibrary _AbilityLibrary;

    [SerializeField] private InputData _InputData;

    public Action<AbilityID> OnAbilityConsumed { get; set; }

    public AbilityRechargeEvent AbilityRechargeEvent;

    public void Start()
    {
        _InputData.Register(this);

        _TimeRemaining = _AbilityQueueData.AbilityRechargeRate;

        _Abilities = new[]
        {
            GetRandomAbilityID(),
            GetRandomAbilityID(),
            GetRandomAbilityID()
        };
    }

    public void OnDestroy()
    {
        _InputData.UnRegister(this);
    }

    private AbilityID GetRandomAbilityID()
    {
        int index = UnityEngine.Random.Range(0, _AbilityLibrary.GetAll().Length);
        return _AbilityLibrary.GetAll()[index];
    }

    /// <summary>Get the next 3 abilities in order they become available</summary>
    /// <returns></returns>
    public AbilityID[] GetNext3Abilities()
    {
        return _Abilities?.Take(3).ToArray() ?? new AbilityID[0];
    }

    /// <summary>Get the current available ability type, if one is available</summary>
    /// <param name="abilityID">The ID of ability to create</param>
    /// <returns>True, if an ability was consumed</returns>
    public Boolean ConsumeAbility(out AbilityID abilityID)
    {
        abilityID = null;

        bool consumeAbility = false;
        if (_AbilityReady)
        {
            abilityID = _Abilities.First();

            for (int i = 0; i < _Abilities.Length - 1; i++)
            {
                _Abilities[i] = _Abilities[i + 1];
            }

            _Abilities[_Abilities.Length - 1] = GetRandomAbilityID();

            _TimeRemaining = _AbilityQueueData.AbilityRechargeRate;

            _AbilityReady = false;
            consumeAbility = true;

            OnAbilityConsumed?.Invoke(abilityID);
        }

        return consumeAbility;
    }

    public void Update()
    {
        //If current ability is already ready, then ignore any timers til that is consumed
        if (_AbilityReady)
        {
            return;
        }

        AbilityID currentAbility = _Abilities.First();

        _TimeRemaining -= Time.deltaTime * Time.timeScale;

        if (_TimeRemaining <= 0)
        {
            _TimeRemaining = 0;

            _AbilityReady = true;

            AbilityRechargeEvent?.Raise(currentAbility, 1f);
        }
        else
        {
            float min = 0f;
            float max = (Single)_AbilityQueueData.AbilityRechargeRate;
            float value = (Single)_TimeRemaining;

            float normalized = MathHelper.NormaliseRange(min, max, value);

            if (Math.Abs(min - max) < 0.05f)
            {
                normalized = 1f;
            }

            AbilityRechargeEvent?.Raise(currentAbility, normalized);
        }
    }

    #region Implementation of IInputListener

    public bool InputEnabled { get; set; } = true;

    public int Order { get; private set; } = 0;

    public bool OnPressed(Vector2 mousePositon, out bool consume)
    {
        if (ConsumeAbility(out AbilityID abilityID))
        {
            Vector2 d = mousePositon - (Vector2)transform.position;
            d.Normalize();

            AbilitySpawnData abilitySpawnData = new AbilitySpawnData
            {
                TargetDirection = d,
                Tag = Constants.Tag.PLAYER,

                AbilityID = abilityID,

                PlayerGameObject = gameObject
            };

            _AbilityLibrary.Create(abilitySpawnData);
        }

        return consume = true;
    }

    #endregion
}
