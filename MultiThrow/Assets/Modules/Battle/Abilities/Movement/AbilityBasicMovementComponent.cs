﻿using System;
using UnityEngine;

public class AbilityBasicMovementComponent : AbilityMovementComponent
{
    [SerializeField] private Vector2 _Direction;

    public void OnEnable()
    {
        _AbilityMover.Add(gameObject);
    }

    public void OnDisable()
    {
        _AbilityMover.Remove(gameObject);
    }

    #region Overrides of AbilityMovementComponent

    public override Vector2 Direction
    {
        get => _Direction;
        set => _Direction = value;
    }

    public override Vector2 GetNextMovement(float deltaTime)
    {
        return (Vector3)(Direction * Data.Speed * deltaTime);
    }

    #endregion

    #region Overrides of AbilityMovementComponent

    protected override void OnUpdated()
    {
        base.OnUpdated();

        bool isUp = tag == Constants.Tag.PLAYER;
        bool movingRight = _Direction.x > 0 && isUp || _Direction.x < 0 && !isUp;

        float x = Math.Abs(transform.localScale.x) * (movingRight ? 1 : -1);

        transform.localScale = new Vector3(x, transform.localScale.y, transform.localScale.z);
    }

    #endregion
}
