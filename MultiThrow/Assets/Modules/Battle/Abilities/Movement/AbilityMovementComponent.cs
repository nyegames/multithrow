﻿using System;
using UnityEngine;

public abstract class AbilityMovementComponent : MonoBehaviour
{
    [SerializeField] public AbilityMovementData Data;

    [SerializeField] protected AbilityMover _AbilityMover;

    private HealthComponent _Health;

    public abstract Vector2 Direction { get; set; }

    public abstract Vector2 GetNextMovement(Single deltaTime);

    public void Start()
    {
        _Health = GetComponent<HealthComponent>();
        OnStarted();
    }

    protected virtual void OnStarted()
    {

    }

    public void Update()
    {
        _AbilityMover?.MoveAbility(gameObject, Time.deltaTime * Time.timeScale);
        if (_Health.Health <= 0)
        {
            //TODO - Emit death event for animation?
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            OnUpdated();
        }
    }

    protected virtual void OnUpdated()
    {

    }
}
