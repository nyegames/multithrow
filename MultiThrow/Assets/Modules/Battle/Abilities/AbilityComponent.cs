﻿using System;
using UnityEngine;

public class AbilityComponent : MonoBehaviour
{
    public AbilityID ID;

    public Single SecondsAlive;

    public void Update()
    {
        SecondsAlive += Time.deltaTime * Time.timeScale;
    }
}
