﻿using System;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] private HealthData _Data;

    [SerializeField] Int32 _Health;
    public Int32 Health
    {
        get => _Health;
        set
        {
            int t = Health;
            _Health = value;
            OnHealthChanged?.Invoke(t, Health);
        }
    }

    public ElementType Element;

    public Action<Int32, Int32> OnHealthChanged;

    public void Awake()
    {
        _Health = _Data.Health;
    }

    public Int32 GetRawHealth() => _Data.Health;
}

