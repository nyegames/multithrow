﻿using Assets.Scripts.Data.Events.Powers;
using System;
using UnityEngine;

/// <summary>Read the timeline and create entities at the given location of the simulated player when prompted</summary>
public class TimelineRepeaterComponent : MonoBehaviour
{
    private TimeSpan _Duration = TimeSpan.Zero;

    private Int32 _ConsumedIndex;

    public PowerTriggeredEvent PowerTriggeredEvent;

    public PowerConsumedEvent PowerConsumedEvent;

    [SerializeField] private PowerLibrary _PowerLibrary;
    [SerializeField] private AbilityLibrary _AbilityLibrary;

    [SerializeField] private PlayerProfile _PlayerProfile;

    private TimelineStep[] _Steps;

    private void Start()
    {
        _Steps = _PlayerProfile.Simulation.timeline;
    }

    public void Update()
    {
        if (_Steps == null) return;

        _Duration += TimeSpan.FromSeconds(Time.deltaTime * Time.timeScale);

        for (int i = _ConsumedIndex; i < _Steps.Length; i++)
        {
            TimelineStep timelineStep = _Steps[i];

            if (_Duration.TotalSeconds < timelineStep.StartTime) continue;

            _ConsumedIndex = i + 1;

            TriggerStep(timelineStep);
        }
    }

    private void TriggerStep(TimelineStep timelineStep)
    {
        Vector2 targetDirection = new Vector2(timelineStep.TargetDirection.X * -1, -Math.Abs(timelineStep.TargetDirection.Y));
        targetDirection.Normalize();

        if (!string.IsNullOrEmpty(timelineStep.AbilityCreatedID))
        {
            AbilitySpawnData abilitySpawnData = new AbilitySpawnData
            {
                PlayerGameObject = gameObject,
                AbilityID = _AbilityLibrary.Get(timelineStep.AbilityCreatedID),
                Tag = Constants.Tag.SIMULATION,
                TargetDirection = targetDirection,
            };

            _AbilityLibrary.Create(abilitySpawnData);
        }

        if (!string.IsNullOrEmpty(timelineStep.PowerCreatedID))
        {
            PowerID powerID = _PowerLibrary.Get(timelineStep.PowerCreatedID);
            PowerSpawnData powerSpawnData = new PowerSpawnData
            {
                PowerID = powerID,
                PlayerTag = Constants.Tag.SIMULATION,
                Position = timelineStep.SpawnPosition.ToVector2(),
            };
            _PowerLibrary.CreateTrigger(powerSpawnData);
        }

        if (!string.IsNullOrEmpty(timelineStep.PowerConsumedID))
        {
            PowerID powerID = _PowerLibrary.Get(timelineStep.PowerConsumedID);
            PowerConsumedEvent?.Raise(Constants.Tag.SIMULATION, powerID, timelineStep.SpawnPosition.ToVector2());
        }

        if (!string.IsNullOrEmpty(timelineStep.PowerTriggeredID))
        {
            PowerID firstOrDefault = _PowerLibrary.Get(timelineStep.PowerTriggeredID);
            Vector2 vector2 = timelineStep.SpawnPosition.ToVector2();

            PowerTriggeredEvent?.Raise(Constants.Tag.SIMULATION, firstOrDefault, vector2, timelineStep.DataPayload);
        }
    }
}

