﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SimulationRecorderComponent : MonoBehaviour
{
    private Single _ElapsedTime;

    public List<TimelineStep> Timeline { get; private set; } = new List<TimelineStep>();

    public void Start()
    {

    }

    public void Stop()
    {

    }

    #region Implementation of IUpdatable

    public void Update()
    {
        _ElapsedTime += Time.deltaTime * Time.timeScale;
    }

    #endregion

    public void OnAbilityCreated(AbilityCreationEventData eventData)
    {
        if (tag == Constants.Tag.SIMULATION) return;

        TimelineStep step = new TimelineStep
        {
            StartTime = _ElapsedTime,
            AbilityCreatedID = eventData.AbilityID.name,
            SpawnPosition = eventData.Spawn,
            TargetDirection = eventData.Direction,
        };

        Timeline.Add(step);

    }

    public void OnPowerConsumed(String powerTag, PowerID powerID, Vector2 spawn)
    {
        if (powerTag == Constants.Tag.SIMULATION) return;

        TimelineStep step = new TimelineStep
        {
            PowerConsumedID = powerID.name,
            //Flip the Y as the simulation is the other side of the screen
            SpawnPosition = new JsonVector2
            {
                X = spawn.x,
                Y = -spawn.y
            },
            StartTime = _ElapsedTime
        };
        Timeline.Add(step);
    }

    public void OnPowerTriggered(String powerTag, PowerID powerid, Vector2 spawn, String powerData)
    {
        if (powerTag == Constants.Tag.SIMULATION) return;

        TimelineStep step = new TimelineStep
        {
            PowerTriggeredID = powerid.name,
            SpawnPosition = spawn.ToJsonVector2(),
            StartTime = _ElapsedTime,
            DataPayload = powerData
        };
        Timeline.Add(step);
    }

    public void OnPowerCreated(PowerCreatedEventData eventData)
    {
        if (eventData.Tag == Constants.Tag.SIMULATION) return;

        Vector2 spawn = eventData.Spawn.ToVector2();

        TimelineStep step = new TimelineStep
        {
            PowerCreatedID = eventData.Power.name,
            //Flip the Y as the simulation is the other side of the screen
            SpawnPosition = new JsonVector2
            {
                X = spawn.x,
                Y = -spawn.y
            },
            StartTime = _ElapsedTime,
        };
        Timeline.Add(step);
    }
}

