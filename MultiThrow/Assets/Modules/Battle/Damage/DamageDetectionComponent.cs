﻿using System;
using UnityEngine;

public abstract class DamageDetectionComponent : MonoBehaviour
{
    public abstract void OnDamageReceived(Int32 damage, ElementType element, Vector2 position);
}