﻿using UnityEngine;

[CreateAssetMenu(menuName = "Battle/PlayArea")]
public class PlayArea : ScriptableObject
{
    [SerializeField] private Vector2 _Size = new Vector2(12, 30);
    [SerializeField] private Vector2 _Position = new Vector2(0, 0);
    
    public Rect Area
    {
        get { return new Rect((Vector2) _Position, size: _Size); }
    }
}