﻿using Glide;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class BattleState : MonoBehaviour
{
    private SimulationInformation _Simulation;

    private HealthComponent _PlayerHealth;
    private HealthComponent _SimulationHealth;

    [SerializeField] private Boolean _RoundEnd;
    private Boolean _RoundCompletionInProgress;

    private Tweener _EndGameTweener;

    public AbilityLibrary _AbilityLibrary;

    [SerializeField] private PlayerProfile _PlayerProfile;
    [SerializeField] private GameDatabase _Database;
    [SerializeField] private PlayArea _PlayArea;
    [SerializeField] private GameRoundDataModel _DataModel;
    [SerializeField] private AbilityMover _AbilityMover;
    [SerializeField] private InputData _InputData;

    //TODO - Replace this dependancy with events the PowerSystem can listen to to disable itself
    [SerializeField] private PowerSystem _PowerSystem;

    public Single TimeScale
    {
        get => Time.timeScale;
        set => Time.timeScale = value;
    }

    public void OnDrawGizmos()
    {
        if (_PlayArea == null) return;
        GizmoHelper.DrawRectGizmo(_PlayArea.Area, Color.yellow);
    }

    public void Start()
    {
        _RoundCompletionInProgress = false;
        _Simulation = _PlayerProfile.Simulation;

        #region Generate Temp Sim

        if (_Simulation?.timeline == null || !_Simulation.timeline.Any())
        {
            Single abilityCooldown = 2f;

            List<TimelineStep> steps = new List<TimelineStep>();
            for (int i = 0; i < 15; i++)
            {
                Vector2 start = new Vector2(0, 0);
                int radius = 20;
                float angle = (float)(Random.Range(0, 1f) * (Math.PI * 2f));
                Vector2 end = new Vector2(start.x + (Single)radius * (Single)Math.Cos(angle), start.y + radius * (Single)Math.Sin(angle));

                Vector2 targetDirection = start - end;
                targetDirection.Normalize();

                steps.Add(new TimelineStep
                {
                    StartTime = (Single)abilityCooldown + (Single)i * (Single)abilityCooldown,
                    AbilityCreatedID = _AbilityLibrary.GetAll().RandomItem().name,
                    TargetDirection = targetDirection.ToJsonVector2()
                });
            }

            _Simulation = new SimulationInformation
            {
                simulation_id = Guid.NewGuid().ToString(),
                timeline = steps.ToArray(),

                owner_player = new PlayerProfileData
                {
                    rank = _PlayerProfile.Rank,
                    exp = _PlayerProfile.Exp,
                    player_id = _PlayerProfile.ID
                },

                duration = 30
            };

            _PlayerProfile.Simulation = _Simulation;
        }

        #endregion

        _AbilityMover.AllowCollisions(true);

        //Always get 30 seconds to attempt any simulation
        _Simulation.duration = 30;

        HealthComponent[] healths = FindObjectsOfType<HealthComponent>();
        _PlayerHealth = healths.First(s => s.tag == Constants.Tag.PLAYER);
        _SimulationHealth = healths.First(s => s.tag == Constants.Tag.SIMULATION);

        _RoundEnd = false;
        _DataModel.Play(_Simulation.duration);

        _PowerSystem.enabled = true;
    }

    public void OnDestroy()
    {
        _EndGameTweener?.Cancel();
        _EndGameTweener = null;
    }

    public void Update()
    {
        //Do not use scale time here, as it needs to update this tweener to decrease the time scale
        _EndGameTweener?.Update(Time.deltaTime);

        if (_PlayerHealth.Health <= 0 || _SimulationHealth.Health <= 0)
        {
            _RoundEnd = true;
        }

        if (!_RoundEnd && _DataModel.GetSecondsRemaining() > 0)
        {
            _DataModel.Tick(Time.deltaTime * Time.timeScale);
        }
        else if (_RoundEnd && !_RoundCompletionInProgress)
        {
            _RoundCompletionInProgress = true;
            StartCoroutine(EndOfRound(_Simulation));
        }
    }

    public void EndOfRound(GameRoundDataModel model)
    {
        _RoundEnd = true;
    }

    private IEnumerator EndOfRound(SimulationInformation simulationInformation)
    {
        //Disable the game for a moment to stop anything happening while we transition out
        if (_InputData != null)
        {
            _InputData.Disabled = true;
            _InputData.ClearListeners();
        }

        _PowerSystem.enabled = false;

        _AbilityMover.AllowCollisions(false);

        _EndGameTweener = new Tweener();
        _EndGameTweener.Tween(this, new { TimeScale = 0f }, 0.5f);

        GameObject playerEntity = _PlayerHealth.gameObject;
        SimulationRecorderComponent simRecorder = playerEntity.GetComponent<SimulationRecorderComponent>();
        HealthComponent playerHealth = playerEntity.GetComponent<HealthComponent>();

        GameObject simPlayer = _SimulationHealth.gameObject;
        HealthComponent simulatedHealth = simPlayer.GetComponent<HealthComponent>();

        //You have killed the simulation, without dying yourself
        if (simulatedHealth.Health <= 0 && playerHealth.Health > 0 && simRecorder.Timeline.Any())
        {
            //If you win, then clear the current simulation (so you will get a new one)
            _PlayerProfile.Simulation = null;

            TimelineStep[] timelineSteps = simRecorder.Timeline.ToArray();
            float duration = timelineSteps.Max(s => s.StartTime);
            SimulationInformation playerRecording = new SimulationInformation
            {
                simulation_id = Guid.NewGuid().ToString(),
                duration = duration,

                owner_player = new PlayerProfileData
                {
                    rank = _PlayerProfile.Rank,
                    exp = _PlayerProfile.Exp,
                    player_id = _PlayerProfile.ID
                },

                timeline = timelineSteps,
            };

            _PlayerProfile.UpdateRank(simulationInformation);

            string content = JsonUtility.ToJson(playerRecording);
            PostDatabaseRequestAsync<SimulationInformation> postSim = _Database.PostRequest<SimulationInformation>(_Database.SaveSimQuery, content);
            yield return postSim;

            yield return PostEndGame(true);
        }
        else
        {
            yield return PostEndGame(true);
        }
    }

    private IEnumerator PostEndGame(Boolean won)
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + 1f)
        {
            yield return null;
        }

        if (_InputData != null)
        {
            _InputData.Disabled = false;
        }
        Time.timeScale = 1f;

        //TODO - How to unload the current scene
        SceneManager.LoadScene(won ? 1 : 2);
    }
}

