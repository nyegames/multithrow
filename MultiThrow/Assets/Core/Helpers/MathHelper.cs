﻿using System;

public static class MathHelper
{
    public static Single NormaliseRange(Int32 min, Int32 max, Int32 current)
    {
        return ((Single)current - min) / ((Single)max - min);
    }

    public static Single NormaliseRange(Int64 min, Int64 max, Int64 current)
    {
        return ((Single)current - min) / ((Single)max - min);
    }

    public static Single NormaliseRange(Single min, Single max, Single current)
    {
        return ((Single)current - min) / ((Single)max - min);
    }

    public static Double NormaliseRange(Double min, Double max, Double current)
    {
        return (current - min) / (max - min);
    }
}