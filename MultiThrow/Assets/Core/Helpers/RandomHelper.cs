﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class RandomHelper
{
    public static T RandomItem<T>(this IEnumerable<T> collection)
    {
        T[] array = collection.ToArray();
        return array[Random.Range(0, array.Length)];
    }
}