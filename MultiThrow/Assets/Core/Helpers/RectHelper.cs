﻿using System;
using UnityEngine;

public static class RectHelper
{
    public static Rect ReduceRect(Rect orginal, Single widthPercent, Single heightPercent)
    {
        Vector2 size = new Vector2(orginal.width * widthPercent, orginal.height * heightPercent);
        Vector2 tl = new Vector2(orginal.x + (orginal.width - size.x) / 2f, orginal.y - (orginal.height - size.y) / 2f);
        return new Rect(tl, size);
    }
}
