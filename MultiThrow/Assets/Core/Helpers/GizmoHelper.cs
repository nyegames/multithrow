﻿using UnityEngine;

public static class GizmoHelper
{
    public static void DrawRectGizmo(Rect rect, Color col, Vector2 offset = default(Vector2))
    {
        Vector2 tl = new Vector2(rect.xMin, rect.y);
        Vector2 tr = new Vector2(rect.xMax, rect.y);
        Vector2 br = new Vector2(rect.xMax, rect.yMin - rect.size.y);
        Vector2 bl = new Vector2(rect.xMin, rect.yMin - rect.size.y);

        Vector2[] edgePoints = new Vector2[]
        {
                tl,tr,br,bl,tl
        };

        for (int i = 0; i < edgePoints.Length - 1; i++)
        {
            Gizmos.color = col;
            Gizmos.DrawLine(edgePoints[i], edgePoints[i + 1]);
        }
    }
}
