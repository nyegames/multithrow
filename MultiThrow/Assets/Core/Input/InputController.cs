﻿using UnityEngine;

namespace Assets.Core.Input
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] private InputData _InputData;

        public void Update()
        {
            if (_InputData == null || _InputData.Disabled) return;
            _InputData.Update();
        }
    }
}
