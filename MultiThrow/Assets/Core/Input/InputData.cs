﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "InputData")]
public class InputData : ScriptableObject
{
    [SerializeField] private Int32 _ListenerCount;

    private List<IInputListener> _InputListeners = new List<IInputListener>();

    [SerializeField] private bool _Disabled;

    public Boolean Disabled
    {
        get => _Disabled;
        set => _Disabled = value;
    }

    public void ClearListeners()
    {
        _InputListeners.Clear();
    }

    public void Register(IInputListener item)
    {
        if (_InputListeners.Contains(item)) return;
        _InputListeners.Add(item);
        _InputListeners = _InputListeners.OrderBy(s => s.Order).ToList();
        _ListenerCount = _InputListeners.Count;
    }

    public void UnRegister(IInputListener item)
    {
        if (!_InputListeners.Contains(item)) return;
        _InputListeners.Remove(item);
        _InputListeners = _InputListeners.OrderBy(s => s.Order).ToList();
        _ListenerCount = _InputListeners.Count;
    }

    // Update is called once per frame
    public void Update()
    {
        if (Disabled) return;
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (int i = _InputListeners.Count - 1; i >= 0; i--)
            {
                IInputListener inputListener = _InputListeners[i];
                if (!inputListener.InputEnabled)
                {
                    continue;
                }

                if (inputListener.OnPressed(pos, out Boolean consume) && consume)
                {
                    break;
                }
            }
        }
    }
}

public interface IInputListener
{
    Boolean InputEnabled { get; set; }

    Int32 Order { get; }

    Boolean OnPressed(Vector2 mousePositon, out Boolean consume);
}
