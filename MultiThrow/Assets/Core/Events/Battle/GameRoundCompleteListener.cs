﻿using UnityEngine;

public class GameRoundCompleteListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameRoundCompleteEvent Event;

    [Tooltip("Response to invoke when Event is raised. HPFrom -> HPNow")]
    public GameRoundCompleteUnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(GameRoundDataModel model)
    {
        Response.Invoke(model);
    }
}