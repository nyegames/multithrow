﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Events/Battle/GameRoundComplete")]
public class GameRoundCompleteEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GameRoundCompleteListener> _EventListeners =
        new List<GameRoundCompleteListener>();

    public virtual void Raise(GameRoundDataModel model)
    {
        for (int i = _EventListeners.Count - 1; i >= 0; i--)
        {
            if (i >= _EventListeners.Count)
            {
                continue;
            }
            _EventListeners[i].OnEventRaised(model);
        }
    }

    public void RegisterListener(GameRoundCompleteListener listener)
    {
        if (!_EventListeners.Contains(listener))
            _EventListeners.Add(listener);
    }

    public void UnregisterListener(GameRoundCompleteListener listener)
    {
        if (_EventListeners.Contains(listener))
            _EventListeners.Remove(listener);
    }
}

/// <summary>
/// Int32 - Previous HP,
/// Int32 - Current HP,
/// String - Player Tag
/// </summary>
[Serializable]
public class GameRoundCompleteUnityEvent : UnityEvent<GameRoundDataModel>
{

}