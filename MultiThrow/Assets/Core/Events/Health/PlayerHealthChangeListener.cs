﻿using System;
using UnityEngine;

public class PlayerHealthChangeListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public PlayerHealthChangedEvent Event;

    [Tooltip("Response to invoke when Event is raised. HPFrom -> HPNow")]
    public PlayerHealthChangedUnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Int32 hpThen, Int32 hpNow, String tag)
    {
        Response.Invoke(hpThen, hpNow, tag);
    }
}