﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Events/Health/PlayerHealthEvent")]
public class PlayerHealthChangedEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<PlayerHealthChangeListener> _EventListeners =
        new List<PlayerHealthChangeListener>();

    public virtual void Raise(Int32 hpThen, Int32 hpNow, String tag)
    {
        for (int i = _EventListeners.Count - 1; i >= 0; i--)
        {
            if (i >= _EventListeners.Count)
            {
                continue;
            }
            _EventListeners[i].OnEventRaised(hpThen, hpNow, tag);
        }
    }

    public void RegisterListener(PlayerHealthChangeListener listener)
    {
        if (!_EventListeners.Contains(listener))
            _EventListeners.Add(listener);
    }

    public void UnregisterListener(PlayerHealthChangeListener listener)
    {
        if (_EventListeners.Contains(listener))
            _EventListeners.Remove(listener);
    }
}

/// <summary>
/// Int32 - Previous HP,
/// Int32 - Current HP,
/// String - Player Tag
/// </summary>
[Serializable]
public class PlayerHealthChangedUnityEvent : UnityEvent<Int32, Int32, String>
{

}