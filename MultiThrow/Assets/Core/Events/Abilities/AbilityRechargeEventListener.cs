﻿using System;
using UnityEngine;

public class AbilityRechargeEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public AbilityRechargeEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public AbilityRechargeUnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(AbilityID id, Single recharge)
    {
        Response.Invoke(id, recharge);
    }
}