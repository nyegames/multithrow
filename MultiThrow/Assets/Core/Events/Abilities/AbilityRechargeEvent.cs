﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Events/Abilities/AbilityRecharge")]
public class AbilityRechargeEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<AbilityRechargeEventListener> _EventListeners =
        new List<AbilityRechargeEventListener>();

    public virtual void Raise(AbilityID id, Single recharge)
    {
        for (int i = _EventListeners.Count - 1; i >= 0; i--)
        {
            if (i >= _EventListeners.Count)
            {
                continue;
            }
            _EventListeners[i].OnEventRaised(id, recharge);
        }
    }

    public void RegisterListener(AbilityRechargeEventListener listener)
    {
        if (!_EventListeners.Contains(listener))
            _EventListeners.Add(listener);
    }

    public void UnregisterListener(AbilityRechargeEventListener listener)
    {
        if (_EventListeners.Contains(listener))
            _EventListeners.Remove(listener);
    }
}

[Serializable]
public class AbilityRechargeUnityEvent : UnityEvent<AbilityID, Single>
{

}