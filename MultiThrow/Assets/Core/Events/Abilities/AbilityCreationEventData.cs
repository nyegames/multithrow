﻿using System;

[Serializable]
public class AbilityCreationEventData
{
    public String Tag;
    public AbilityID AbilityID;
    public JsonVector2 Spawn;
    public JsonVector2 Direction;
    public Int32 InstanceID;
}