﻿using UnityEngine;

public class AbilityCreationEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public AbilityCreatedEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public AbilityCreationUnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(AbilityCreationEventData eventData)
    {
        Response.Invoke(eventData);
    }
}