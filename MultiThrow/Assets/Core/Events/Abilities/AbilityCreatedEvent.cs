﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[CreateAssetMenu(menuName = "Events/Abilities/CreationEvent")]
public class AbilityCreatedEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    [SerializeField]
    private readonly List<AbilityCreationEventListener> _EventListeners = new List<AbilityCreationEventListener>();

    public void Raise(AbilityCreationEventData eventData)
    {
        for (int i = _EventListeners.Count - 1; i >= 0; i--)
        {
            //Safety for if an event triggers multiple removals of listeners in 1 trigger
            if (i >= _EventListeners.Count)
            {
                continue;
            }

            _EventListeners[i].OnEventRaised(eventData);
        }

    }

    public void RegisterListener(AbilityCreationEventListener listener)
    {
        if (!_EventListeners.Contains(listener))
            _EventListeners.Add(listener);
    }

    public void UnregisterListener(AbilityCreationEventListener listener)
    {
        if (_EventListeners.Contains(listener))
            _EventListeners.Remove(listener);
    }
}

/// <summary>
/// String - Tag,
/// String - AbilityID,
/// Vector2 - SpawnPosition,
/// Vector2 - TargetDirection
/// </summary>
[Serializable]
public class AbilityCreationUnityEvent : UnityEvent<AbilityCreationEventData>
{

}