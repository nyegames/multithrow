﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Data.Events.Powers
{
    [CreateAssetMenu(menuName = "Events/Powers/TriggeredEvent")]
    public class PowerTriggeredEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<PowerTriggeredEventListener> _EventListeners =
            new List<PowerTriggeredEventListener>();

        public void Raise(String tag, PowerID powerid, Vector2 spawn, String payload)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(tag, powerid, spawn, payload);
            }
        }

        public void RegisterListener(PowerTriggeredEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(PowerTriggeredEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }

    /// <summary>
    /// String - Tag,
    /// PowerID - PowerID,
    /// Vector2 - SpawnPosition,
    /// String - PowerExtraJsonPayload
    /// </summary>
    [Serializable]
    public class PowerTriggeredUnityEvent : UnityEvent<String, PowerID, Vector2, String>
    {

    }
}
