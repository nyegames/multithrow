﻿using System;
using UnityEngine;

namespace Assets.Scripts.Data.Events.Powers
{
    public class PowerTriggeredEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public PowerTriggeredEvent Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public PowerTriggeredUnityEvent Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised(String powerTag, PowerID powerid, Vector2 spawn, string payload)
        {
            Response.Invoke(powerTag, powerid, spawn, payload);
        }
    }
}