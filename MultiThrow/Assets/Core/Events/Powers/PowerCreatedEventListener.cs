﻿using System;
using UnityEngine;

public class PowerCreatedEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public PowerCreatedEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public PowerCreatedUnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(PowerCreatedEventData eventData)
    {
        Response.Invoke(eventData);
    }
}