﻿using System;

[Serializable]
public class PowerCreatedEventData
{
    public String Tag;
    public PowerID Power;
    public JsonVector2 Spawn;
    public Int32 InstanceID;
}