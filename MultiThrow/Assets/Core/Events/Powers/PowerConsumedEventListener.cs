﻿using System;
using UnityEngine;

public class PowerConsumedEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public PowerConsumedEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public PowerConsumedUnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(String powerTag, PowerID powerid, Vector2 spawn)
    {
        Response.Invoke(powerTag, powerid, spawn);
    }
}