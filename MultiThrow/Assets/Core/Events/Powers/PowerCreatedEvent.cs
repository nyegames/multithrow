﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Events/Powers/CreatedEvent")]
public class PowerCreatedEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<PowerCreatedEventListener> _EventListeners =
        new List<PowerCreatedEventListener>();

    public void Raise(PowerCreatedEventData eventData)
    {
        for (int i = _EventListeners.Count - 1; i >= 0; i--)
        {
            if (i >= _EventListeners.Count)
            {
                continue;
            }
            _EventListeners[i].OnEventRaised(eventData);
        }
    }

    public void RegisterListener(PowerCreatedEventListener listener)
    {
        if (!_EventListeners.Contains(listener))
            _EventListeners.Add(listener);
    }

    public void UnregisterListener(PowerCreatedEventListener listener)
    {
        if (_EventListeners.Contains(listener))
            _EventListeners.Remove(listener);
    }
}

/// <summary>
/// String - Tag,
/// PowerID - PowerID,
/// Vector2 - SpawnPosition,
/// String - PowerExtraJsonPayload
/// </summary>
[Serializable]
public class PowerCreatedUnityEvent : UnityEvent<PowerCreatedEventData>
{

}