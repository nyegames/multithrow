﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Events/Powers/ConsumedEvent")]
public class PowerConsumedEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<PowerConsumedEventListener> _EventListeners =
        new List<PowerConsumedEventListener>();

    public void Raise(String tag, PowerID powerid, Vector2 spawn)
    {
        for (int i = _EventListeners.Count - 1; i >= 0; i--)
        {
            if (i >= _EventListeners.Count)
            {
                continue;
            }
            _EventListeners[i].OnEventRaised(tag, powerid, spawn);
        }
    }

    public void RegisterListener(PowerConsumedEventListener listener)
    {
        if (!_EventListeners.Contains(listener))
            _EventListeners.Add(listener);
    }

    public void UnregisterListener(PowerConsumedEventListener listener)
    {
        if (_EventListeners.Contains(listener))
            _EventListeners.Remove(listener);
    }
}

/// <summary>
/// String - Tag,
/// PowerID - PowerID,
/// Vector2 - SpawnPosition,
/// String - PowerExtraJsonPayload
/// </summary>
[Serializable]
public class PowerConsumedUnityEvent : UnityEvent<String, PowerID, Vector2>
{

}