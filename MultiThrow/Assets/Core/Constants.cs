﻿using System;

public class Constants
{
    public static class Tag
    {
        public const String PLAYER = "MyPlayer";

        public const String SIMULATION = "SimulatedPlayer";
    }

    public static class Layer
    {
        public const Int32 PLAYER = 8;

        public const Int32 SIMULATION = 9;

        public const Int32 PLAYER_POWERS = 10;

        public const Int32 SIMULATED_POWERS = 11;
    }
}


