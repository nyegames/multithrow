﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraAspect : MonoBehaviour
{
    public Vector2 TargetAspectRatio;
    private Camera _Camera;
    private float _RectHeight;
    private float _Windowaspect;

    // Use this for initialization
    void Start()
    {
        // obtain camera component so we can modify its viewport
        _Camera = GetComponent<Camera>();

        LetterBox();
    }

    private void LetterBox()
    {
        // set the desired aspect ratio (the values in this example are
        // hard-coded for 16:9, but you could make them into public
        // variables instead so you can set them at design time)
        float targetaspect = TargetAspectRatio.x / TargetAspectRatio.y;

        // determine the game window's current aspect ratio
        _Windowaspect = (float)Screen.width / (float)Screen.height;

        // current viewport height should be scaled by this amount
        _RectHeight = _Windowaspect / targetaspect;

        // if scaled height is less than current height, add letterbox
        if (_RectHeight < 1.0f)
        {
            Rect rect = _Camera.rect;

            rect.width = 1.0f;
            rect.height = _RectHeight;
            rect.x = 0;
            rect.y = (1.0f - _RectHeight) / 2.0f;

            _Camera.rect = rect;
        }
        else // add pillarbox
        {
            float scalewidth = 1.0f / _RectHeight;

            Rect rect = _Camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            _Camera.rect = rect;
        }
    }

    public void Update()
    {
        LetterBox();

#if UNITY_EDITOR
        LetterBox();
#endif
    }
}
