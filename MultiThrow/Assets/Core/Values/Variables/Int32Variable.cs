﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "IntVariable", menuName = "Variables/Int32")]
public class Int32Variable : ScriptableObject
{
    public Int32 Value;
}
