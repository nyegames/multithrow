﻿using System;

[Serializable]
public class Int32Reference
{
    public bool UseConstant = true;
    public Int32 ConstantValue;
    public Int32Variable Variable;

    public Int32Reference()
    { }

    public Int32Reference(Int32 value)
    {
        UseConstant = true;
        ConstantValue = value;
    }

    public Int32 Value => UseConstant ? ConstantValue : Variable.Value;

    public static implicit operator Int32(Int32Reference reference)
    {
        return reference.Value;
    }
}
