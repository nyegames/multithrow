﻿using System;
using System.Globalization;

[Serializable]
public class JsonDateTime
{
    public String DateTime;

    public DateTime ToDateTime()
    {
        return System.DateTime.Parse(DateTime);
    }

    public static JsonDateTime FromDateTime(DateTime datetime)
    {
        return new JsonDateTime
        {
            DateTime = datetime.ToString(CultureInfo.InvariantCulture)
        };
    }
}
