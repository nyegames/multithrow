﻿using System;
using UnityEngine;

[Serializable]
public class JsonVector2
{
    public Single X;
    public Single Y;

    public Vector2 ToVector2()
    {
        return new Vector2(X, Y);
    }
}

public static class JsonVector2Ext
{
    public static JsonVector2 ToJsonVector2(this Vector2 vector)
    {
        return new JsonVector2
        {
            X = vector.x,
            Y = vector.y
        };
    }

    public static JsonVector2 ToJsonVector2(this Vector3 vector)
    {
        return new JsonVector2
        {
            X = vector.x,
            Y = vector.y
        };
    }
}
