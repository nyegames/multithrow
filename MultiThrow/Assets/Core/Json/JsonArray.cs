﻿using System;
using UnityEngine;

//https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity
//Unity Json is terrible and cannot handle Arrays

//http://wiki.unity3d.com/index.php/SimpleJSON
//Use this if dynamic type behaviour is required

public static class JsonArray
{
    public static T[] FromJson<T>(string json)
    {
        json = "{\"Items\":" + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array, bool prettyPrint = false)
    {
        Wrapper<T> wrapper = new Wrapper<T> { Items = array };
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}