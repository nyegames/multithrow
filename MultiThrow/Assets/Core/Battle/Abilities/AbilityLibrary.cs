﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/Library")]
public class AbilityLibrary : ScriptableObject
{
    [SerializeField] private List<AbilityID> _Abilities;

    [SerializeField] private List<GameObject> _AbilityPrefabs;

    [SerializeField] private AbilityCreatedEvent _AbilityCreationEvent;

    public AbilityID Get(String abilityName)
    {
        AbilityID id = _Abilities.FirstOrDefault(s => s.name == abilityName);
        return id;
    }

    public AbilityID[] GetAll() => _Abilities?.ToArray();

    public void Create(AbilitySpawnData data)
    {
        //All abilities to be named "Example-{ID}"
        GameObject ability = _AbilityPrefabs.First(s => data.AbilityID == s.GetComponent<AbilityComponent>().ID);
        GameObject gameObj = Instantiate(ability, data.PlayerGameObject.transform.position, Quaternion.identity);
        if (gameObj == null)
        {
            return;
        }

        gameObj.transform.position = data.PlayerGameObject.transform.position;

        gameObj.tag = data.PlayerGameObject.tag;

        gameObj.layer = data.PlayerGameObject.layer;

        AbilityMovementComponent mover = gameObj.GetComponent<AbilityMovementComponent>();
        mover.Direction = mover.Data.InitialDirection = data.TargetDirection;

        gameObj.transform.rotation = Quaternion.Euler(0, 0, gameObj.tag == Constants.Tag.PLAYER ? 0 : 180);

        AbilityCreationEventData eventData = new AbilityCreationEventData()
        {
            Tag = data.Tag,
            AbilityID = data.AbilityID,
            Spawn = gameObj.transform.position.ToJsonVector2(),
            Direction = data.TargetDirection.ToJsonVector2(),
            InstanceID = gameObj.GetInstanceID()
        };
        _AbilityCreationEvent?.Raise(eventData);
    }

    public ElementType GetElement(AbilityID abilityID)
    {
        AbilityComponent[] abilityComponents = _AbilityPrefabs?.Select(s => s.GetComponent<AbilityComponent>()).ToArray();
        GameObject ability = abilityComponents?.FirstOrDefault(s => s.ID == abilityID)?.gameObject;
        DamageComponent damageComponent = ability?.GetComponent<DamageComponent>();
        return damageComponent?.Element;
    }
}
