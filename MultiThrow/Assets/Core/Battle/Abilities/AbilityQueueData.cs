﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "AbilityQueueData", menuName = "ScriptableObjects/Battle/AbilityQueueData")]
public class AbilityQueueData : ScriptableObject
{
    [SerializeField] public Single AbilityRechargeRate;
}
