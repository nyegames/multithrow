﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "AbilityMovementData", menuName = "ScriptableObjects/Battle/AbilityMovementData")]
public class AbilityMovementData : ScriptableObject
{
    [SerializeField] public Single Speed;

    [SerializeField] public Vector2 InitialDirection;
}
