﻿using System;
using UnityEngine;

/// <summary>DATA USED TO CREATE AN ABILITY</summary>
public sealed class AbilitySpawnData
{
    /// <summary>Player or Simulation tag</summary>
    public String Tag;

    public GameObject PlayerGameObject;

    /// <summary>Starting direction to create this Throw from</summary>
    public Vector2 TargetDirection;

    /// <summary>ID of the throw effect used</summary>
    public AbilityID AbilityID;
}