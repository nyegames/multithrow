﻿using System;

namespace Assets.Scripts.Data.Abilities
{
    [Serializable]
    public sealed class AbilityCreationData
    {
        public string Tag;

        public AbilityID AbilityID;

        public JsonVector2 SpawnPosition;

        public JsonVector2 TargetDirection;
    }
}
