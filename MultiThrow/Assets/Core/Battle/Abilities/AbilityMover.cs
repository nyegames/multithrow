﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/AbilityMover")]
public class AbilityMover : ScriptableObject
{
    private Boolean _CollisionsEnabled = true;

    [SerializeField] private PlayArea _PlayArea;

    private List<GameObject> _CurrentActiveAbilities = new List<GameObject>();

    [SerializeField] private PlayerHealthChangedEvent _HealthChangedEvent;

    [SerializeField] private AbilityCreatedEvent _AbilityCreationEvent;

    [ContextMenu("Clear Abilities")]
    public void Clear()
    {
        for (int i = _CurrentActiveAbilities.Count - 1; i >= 0; i--)
        {
            GameObject gameObject = _CurrentActiveAbilities[i];
            HealthComponent hp = gameObject.GetComponent<HealthComponent>();
            if (hp != null)
            {
                hp.Health = 0;
            }
        }

        _CurrentActiveAbilities.Clear();
    }

    public void Add(GameObject ability)
    {
        if (_CurrentActiveAbilities.Contains(ability)) return;
        _CurrentActiveAbilities.Add(ability);
    }

    public void Remove(GameObject ability)
    {
        if (!_CurrentActiveAbilities.Contains(ability)) return;
        _CurrentActiveAbilities.Remove(ability);
    }

    public void MoveAbility(GameObject myObj, Single deltaTime)
    {
        AbilityMovementComponent movementComp = myObj.GetComponent<AbilityMovementComponent>();
        HealthComponent myHp = myObj.GetComponent<HealthComponent>();

        bool hitWall = false;

        Rect rect = _PlayArea.Area;

        Vector2 moveMe = movementComp.GetNextMovement(deltaTime);
        movementComp.gameObject.transform.localPosition += (Vector3)moveMe;

        float positionX = movementComp.transform.position.x;
        if (moveMe.x > 0 && positionX > rect.xMax)
        {
            movementComp.Direction = new Vector2(-Mathf.Abs(movementComp.Direction.x), movementComp.Direction.y);
            hitWall = true;
        }
        else if (moveMe.x < 0 && positionX < rect.xMin)
        {
            movementComp.Direction = new Vector2(Mathf.Abs(movementComp.Direction.x), movementComp.Direction.y);
            hitWall = true;
        }

        //Ignore collisions if disabled
        if (!_CollisionsEnabled) return;

        if (hitWall)
        {
            HealthComponent abilityHealth = myObj.GetComponent<HealthComponent>();
            if (abilityHealth != null)
            {
                abilityHealth.Health -= 1;
            }
        }

        float positionY = myObj.transform.position.y;

        //Outside game bounds?
        Rect area = _PlayArea.Area;

        if (
            //Moving up
            positionY > area.yMin ||
            //Moving down
            positionY < area.yMin - area.size.y
        )
        {
            if (myHp != null)
            {
                myHp.Health = 0;
            }
        }
        else if (DetectCollision(myObj, out GameObject otherObject))
        {
            ApplyDamage(myObj, otherObject);
        }
    }

    private Boolean DetectCollision(GameObject myAbility, out GameObject collidedObj)
    {
        collidedObj = null;
        HealthComponent thisAbilityHealth = myAbility.GetComponent<HealthComponent>();

        //Detect Ability v Ability collisions

        BoxCollider2D myCollider = myAbility.GetComponent<BoxCollider2D>();

        /*//Get all the abilities that are not tagged as the same as this ability
        AbilityMovementComponent[] otherAbilities = _MovementComponents.Where(s => s.gameObject.tag != thisAbility.tag).ToArray();

        //Get all the layer names of layers that are not yours, from all the other abilities
        string[] opponentLayers = otherAbilities.Select(s => s.gameObject.layer).Distinct().Select(LayerMask.LayerToName).ToArray();*/

        List<Collider2D> colliders = new List<Collider2D>();
        ContactFilter2D contactFilter2D = new ContactFilter2D
        {
            layerMask = LayerMask.GetMask(new[] { LayerMask.LayerToName(Constants.Layer.PLAYER), LayerMask.LayerToName(Constants.Layer.SIMULATION) }),
            useLayerMask = true
        };
        myCollider.OverlapCollider(contactFilter2D, colliders);

        //Get the first collision that is not against its own tag
        Collider2D collidedWith = colliders.FirstOrDefault(s => s.tag != thisAbilityHealth.tag);
        if (collidedWith == null)
        {
            return false;
        }

        collidedObj = collidedWith.gameObject;

        return true;
    }

    private void ApplyDamage(GameObject attacker, GameObject target)
    {
        HealthComponent attackerHealth = attacker.GetComponent<HealthComponent>();
        DamageComponent attackerDamage = attacker.GetComponent<DamageComponent>();

        HealthComponent targetHealth = target.GetComponent<HealthComponent>();

        int thenHP = targetHealth.Health;

        //If you just attacked a player, then you are dead
        PlayerComponent playerComponent = target.GetComponent<PlayerComponent>();
        if (playerComponent != null)
        {
            targetHealth.Health -= 1;
            attackerHealth.Health = 0;

            _HealthChangedEvent?.Raise(thenHP, targetHealth.Health, target.tag);

            return;
        }

        //Ability vs Ability

        DamageComponent targetDamage = target.GetComponent<DamageComponent>();

        ElementType attackerDamageElement = attackerDamage.Element;
        ElementType targetHealthElement = targetHealth.Element;

        //If both are the same elements, destroy each other
        if (attackerDamageElement == targetHealthElement)
        {
            attackerHealth.Health = 0;
            targetHealth.Health = 0;
        }
        else
        {
            bool attackerWins = attackerDamageElement.DefeatedElements.Contains(targetHealthElement);

            //If the attacking ability wins, then destroy the
            if (attackerWins)
            {
                targetHealth.Health = 0;

                //Loser takes 1 damage
                attackerHealth.Health -= 1;
            }
            else
            {
                attackerHealth.Health = 0;

                //Loser takes 1 damage
                targetHealth.Health -= 1;
            }
        }
    }

    public void AllowCollisions(Boolean enabled)
    {
        _CollisionsEnabled = enabled;
    }
}
