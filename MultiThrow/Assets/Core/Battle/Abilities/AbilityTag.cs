﻿using System;

public sealed class AbilityTag : System.Attribute
{
    public String Name { get; private set; }

    public AbilityTag(String name)
    {
        Name = name;
    }
}
