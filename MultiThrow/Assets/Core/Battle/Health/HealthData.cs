﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthData", menuName = "ScriptableObjects/Battle/HealthData")]
public class HealthData : ScriptableObject
{
    public Int32 Health;

    public ElementType Element;
}
