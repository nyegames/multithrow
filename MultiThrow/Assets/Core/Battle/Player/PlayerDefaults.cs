﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerDefaults", menuName = "ScriptableObjects/Battle/PlayerDefaults")]
public class PlayerDefaults : ScriptableObject
{
    public Int32 StartingDirection;
}
