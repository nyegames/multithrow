﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[CreateAssetMenu(menuName = "Systems/PlayerProfile")]
public class PlayerProfile : ScriptableObject
{
    private static string SavePath => $"{Application.persistentDataPath}/playerProfile.bin";

    public SimulationInformation Simulation;

    [SerializeField] private Int64 _Rank;
    [SerializeField] private Int64 _Exp;
    [SerializeField] private String _PlayerID;

    public Int64 Rank => _Rank;
    public Int64 Exp => _Exp;
    public string ID => _PlayerID;

    public void OnEnable()
    {
        if (File.Exists(SavePath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream stream = new FileStream(SavePath, FileMode.OpenOrCreate))
            {
                if (formatter.Deserialize(stream) is PlayerProfileData save)
                {
                    _Rank = save.rank;
                    _Exp = save.exp;
                    _PlayerID = save.player_id;
                }
            }
        }

        if (string.IsNullOrEmpty(_PlayerID))
        {
            _PlayerID = Guid.NewGuid().ToString();
        }
    }

    public void OnDisable()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(SavePath, FileMode.OpenOrCreate))
        {
            PlayerProfileData data = new PlayerProfileData()
            {
                rank = _Rank,
                exp = _Exp,
                player_id = _PlayerID
            };
            formatter.Serialize(stream, data);
        }
    }

    //TODO - Make a leveling system which handles how the player should level up, and any unlocks they get
    public void UpdateRank(SimulationInformation simulation)
    {
        _Exp += 1;

        if (simulation.owner_player.rank >= _Rank)
        {
            _Rank += 1;
        }
    }
}