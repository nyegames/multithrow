﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Battle/GameRoundModel")]
public class GameRoundDataModel : ScriptableObject
{
    private Boolean _Started;
    private Boolean _Complete;

    [SerializeField] private Single _SecondsRemaining;

    public GameRoundCompleteEvent OnGameRoundComplete;

    public Single GetSecondsRemaining() => _SecondsRemaining;

    public void Play(Single totalSeconds)
    {
        _SecondsRemaining = totalSeconds;
        _Started = true;
        _Complete = false;
    }

    public void Tick(Single delta)
    {
        if (!_Started) return;
        if (_Complete) return;

        _SecondsRemaining -= delta;

        if (_SecondsRemaining <= 0)
        {
            _Complete = true;
            OnGameRoundComplete?.Raise(this);
        }
    }
}
