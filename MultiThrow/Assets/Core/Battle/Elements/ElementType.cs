﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ElementType", menuName = "Elements/ElementType")]
public class ElementType : ScriptableObject
{
    [Tooltip("The elements that this element beats")]
    public List<ElementType> DefeatedElements = new List<ElementType>();
}
