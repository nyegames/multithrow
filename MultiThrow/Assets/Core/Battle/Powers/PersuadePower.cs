﻿using UnityEngine;

[CreateAssetMenu(menuName = "Powers/ID/Persuade")]
public class PersuadePower : PowerID
{
    #region Overrides of PowerID

    public override void TriggerPower(string powerTag, Vector2 spawn, string datapayload)
    {
        AbilityComponent[] abilities = FindObjectsOfType<AbilityComponent>();
        foreach (AbilityComponent abilityComponent in abilities)
        {
            if (abilityComponent.SecondsAlive < 0.25f) continue;
            string abilityTag = abilityComponent.gameObject.tag;
            abilityComponent.gameObject.tag = abilityTag == Constants.Tag.PLAYER ? Constants.Tag.SIMULATION : Constants.Tag.PLAYER;
            abilityComponent.gameObject.layer = abilityTag == Constants.Tag.PLAYER ? Constants.Layer.SIMULATION : Constants.Layer.PLAYER;

            abilityComponent.GetComponent<AbilityMovementComponent>().Direction *= new Vector2(1, -1f);
        }
    }

    #endregion
}
