﻿using UnityEngine;

public abstract class PowerID : ScriptableObject
{
    [SerializeField] public GameObject TriggerPrefab;

    public abstract void TriggerPower(string powerTag, Vector2 spawn, string datapayload);
}
