﻿using System;

public class PowerTag : System.Attribute
{
    public String Name { get; }

    public PowerTag(String name)
    {
        Name = name;
    }
}

