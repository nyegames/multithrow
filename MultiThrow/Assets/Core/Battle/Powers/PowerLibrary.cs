﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Powers/Library")]
public class PowerLibrary : ScriptableObject
{
    [SerializeField] private List<PowerID> _Powers;

    [SerializeField] private PowerCreatedEvent _PowerCreatedEvent;

    public PowerID Get(String abilityName)
    {
        foreach (PowerID powerID in _Powers)
        {
            if (powerID.name == abilityName)
            {
                return powerID;
            }
        }

        return null;
    }

    public PowerID[] GetAll() => _Powers?.ToArray();

    public GameObject CreateTrigger(PowerSpawnData data)
    {
        GameObject prefab = data.PowerID?.TriggerPrefab;
        if (prefab == null)
        {
            return null;
        }

        GameObject powerGameObject = Instantiate(prefab);
        if (powerGameObject == null)
        {
            return null;
        }
        powerGameObject.transform.position = data.Position;

        int powerLayer = data.PlayerTag == Constants.Tag.PLAYER ? Constants.Layer.PLAYER_POWERS : Constants.Layer.SIMULATED_POWERS;

        powerGameObject.tag = data.PlayerTag;
        powerGameObject.layer = powerLayer;
        foreach (Transform childGameObj in powerGameObject.GetComponentsInChildren<Transform>())
        {
            childGameObj.gameObject.tag = data.PlayerTag;
            childGameObj.gameObject.layer = powerLayer;
        }

        PowerCreatedEventData eventData = new PowerCreatedEventData()
        {
            Tag = data.PlayerTag,
            Power = data.PowerID,
            Spawn = data.Position.ToJsonVector2(),
            InstanceID = powerGameObject.GetInstanceID()
        };
        _PowerCreatedEvent?.Raise(eventData);

        return powerGameObject;
    }
}

