﻿using UnityEngine;

[CreateAssetMenu(menuName = "Powers/ID/Blocker")]
public class BlockerPower : PowerID
{
    [SerializeField]
    private GameObject _BlockerPrefab;

    #region Overrides of PowerID

    public override void TriggerPower(string powerTag, Vector2 spawn, string datapayload)
    {
        Vector2 triggerPosition = spawn;

        if (_BlockerPrefab == null) return;

        int powerLayer = powerTag == Constants.Tag.PLAYER
            ? Constants.Layer.PLAYER
            : Constants.Layer.SIMULATION;

        BlockingPower[] currentBlockers = FindObjectsOfType<BlockingPower>();
        foreach (BlockingPower currentBlocker in currentBlockers)
        {
            //Only destroy the blockers that are on your team
            if (currentBlocker.tag != powerTag) continue;
            Destroy(currentBlocker.gameObject);
        }

        Vector2 startPos = new Vector2(-6, triggerPosition.y);
        float initialGap = 0.8f;
        //space between each blocker
        float gap = 2.7f;
        //How many blockers
        int count = 3;
        //Width of each blocker
        float monsterWidth = 1.2f;

        for (int i = 0; i < count; i++)
        {
            Vector2 pos = startPos + new Vector2(initialGap + monsterWidth + (monsterWidth * i) + gap * i, 0);
            GameObject blocker = Instantiate(_BlockerPrefab, pos, Quaternion.identity);
            blocker.tag = powerTag;
            blocker.layer = powerLayer;

            SpriteRenderer[] blockerSprites = blocker.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer spriteRenderer in blockerSprites)
            {
                spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, blocker.tag == Constants.Tag.PLAYER ? 1 : 0.5f);
            }
        }
    }

    #endregion
}
