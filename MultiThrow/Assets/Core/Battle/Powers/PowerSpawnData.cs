﻿using System;
using UnityEngine;

public class PowerSpawnData
{
    /// <summary>This power was spawned by this Player</summary>
    public String PlayerTag;

    public PowerID PowerID;

    public Vector2 Position;
}