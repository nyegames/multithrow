﻿using Assets.Scripts.Data.Abilities;
using System;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Powers/ID/MultiShot")]
public class MultiShotPowerID : PowerID
{
    [SerializeField] private AbilityLibrary _AbilityLibrary;

    #region Overrides of PowerID

    public override void TriggerPower(string powerTag, Vector2 spawn, string datapayload)
    {
        //Simulated Multishots do not need to create the abilities here, as they will have already been spawned via the AbilitySpawn event inside the timeline.
        if (powerTag == Constants.Tag.SIMULATION)
        {
            return;
        }

        AbilityCreationData created = JsonUtility.FromJson<AbilityCreationData>(datapayload);

        GameObject playerGameObject = FindObjectsOfType<PlayerComponent>().First(s => s.tag == powerTag).gameObject;
        AbilitySpawnData leftAbility = new AbilitySpawnData
        {
            PlayerGameObject = playerGameObject,
            Tag = powerTag,
            AbilityID = created.AbilityID,
            TargetDirection = (created.TargetDirection.ToVector2() - new Vector2((Single)Math.Cos(25), (Single)Math.Sin(25))).normalized
        };

        _AbilityLibrary.Create(leftAbility);

        AbilitySpawnData rightAbility = new AbilitySpawnData()
        {
            PlayerGameObject = playerGameObject,
            Tag = powerTag,
            AbilityID = created.AbilityID,
            TargetDirection = (created.TargetDirection.ToVector2() + new Vector2((Single)Math.Cos(25), (Single)Math.Sin(25))).normalized
        };
        _AbilityLibrary.Create(rightAbility);
    }

    #endregion
}
