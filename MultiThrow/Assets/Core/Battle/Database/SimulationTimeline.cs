﻿using System;
using System.Collections.Generic;

[Serializable]
public sealed class SimulationTimeline
{
    public List<TimelineStep> steps;
}

