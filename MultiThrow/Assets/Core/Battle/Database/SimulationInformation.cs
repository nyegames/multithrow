﻿using System;
// ReSharper disable All

[Serializable]
public sealed class SimulationInformation
{
    public String simulation_id;

    public string name;

    public Single duration;

    public PlayerProfileData owner_player;

    public TimelineStep[] timeline;

    public String client_v;
}
