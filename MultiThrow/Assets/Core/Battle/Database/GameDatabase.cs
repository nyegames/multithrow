﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameDatabase", menuName = "Systems/Database")]
public class GameDatabase : ScriptableObject
{
    #region Data

    public String BaseAddress;
    public String APIKEY;

    public String LoginQuery = "/playerinfo";
    public String GetAllSimsQuery = "/simulationinfo";
    public String SaveSimQuery = "/simulationinfo";
    public String GetPlayerQuery = "/playerinfo";

    #endregion

    public void OnEnable()
    {
        DatabaseRequest.APIKEY = APIKEY;
        DatabaseRequest.BaseAddress = BaseAddress;
    }

    public GetDatabaseRequestAsync<T> GetRequest<T>(String query, params URLHeader[] headers) where T : class
    {
        return new GetDatabaseRequestAsync<T>(query, headers);
    }

    public PostDatabaseRequestAsync<T> PostRequest<T>(String query, String content, params URLHeader[] headers)
        where T : class
    {
        return new PostDatabaseRequestAsync<T>(query, content, headers);
    }

    public GetArrayDatabaseRequestAsync<T> GetArrayReqest<T>(string query, params URLHeader[] headers) where T : class
    {
        return new GetArrayDatabaseRequestAsync<T>(query, headers);
    }
}