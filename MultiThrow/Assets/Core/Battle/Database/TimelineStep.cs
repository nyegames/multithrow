﻿using System;
using System.Diagnostics.CodeAnalysis;

/// <summary>A single step inside the timeline</summary>
[Serializable]
[SuppressMessage("ReSharper", "InconsistentNaming")]
public sealed class TimelineStep
{
    /// <summary>After the timeline starts, this step will trigger after this amount of seconds</summary>
    public Single StartTime;

    public JsonVector2 SpawnPosition;

    /// <summary>The directional vector the ability was fired in, flip Y if simulated</summary>
    public JsonVector2 TargetDirection;

    /// <summary>The ability that was created</summary>
    public String AbilityCreatedID;

    /// <summary>The power that was created at this step</summary>
    public String PowerCreatedID;

    /// <summary>The power that was consumed at this step</summary>
    public String PowerConsumedID;

    /// <summary>The power that was trigged at this step</summary>
    public String PowerTriggeredID;

    /// <summary>A Json payload used to store additional data for this timeline step</summary>
    public String DataPayload;
}
