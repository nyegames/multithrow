﻿using System;

[Serializable]
public class PlayerProfileData
{
    // ReSharper disable once InconsistentNaming
    public Int64 rank;

    // ReSharper disable once InconsistentNaming
    public Int64 exp;

    // ReSharper disable once InconsistentNaming
    public String player_id;
}
