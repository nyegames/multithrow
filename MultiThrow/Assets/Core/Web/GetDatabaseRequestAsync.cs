﻿using System;
using System.Threading.Tasks;

public class GetDatabaseRequestAsync<T> : DatabaseRequestAsync<T> where T : class
{
    public GetDatabaseRequestAsync(String query, params URLHeader[] headers) : base(query, headers)
    {
        Task.Run(() =>
        {
            Result = Client.GetResponse<T>();

            Success = Result != null;
            Complete = true;
        });
    }
}