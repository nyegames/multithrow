﻿using System;
using System.Threading.Tasks;

public class PostDatabaseRequestAsync<T> : DatabaseRequestAsync<T> where T : class
{
    public PostDatabaseRequestAsync(String query, String content, params URLHeader[] headers) : base(query, headers)
    {
        Task.Run(() =>
        {
            Result = Client.PostResponse<T>(content);
            Success = Result != null;
            Complete = true;
        });
    }
}