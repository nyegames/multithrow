﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class GetArrayDatabaseRequestAsync<T> : IEnumerator where T : class
{
    protected QueryClient Client { get; private set; }

    public GetArrayDatabaseRequestAsync(String query, params URLHeader[] headers)
    {
        List<URLHeader> lheaders = headers.ToList();
        lheaders.Add(new URLHeader("content-type", "application/json"));
        lheaders.Add(new URLHeader("cache-control", "no-cache"));

        Client = new QueryClient(query, lheaders.ToArray())
        {
            Token = new Tuple<string, string>("x-apikey", DatabaseRequest.APIKEY),
            BaseAddress = DatabaseRequest.BaseAddress
        };

        Task.Run(() =>
        {
            Result = Client.GetArrayResponse<T>();

            Success = Result != null;
            Complete = true;
        });
    }

    public Boolean Complete { get; protected set; }

    public Boolean Success { get; protected set; }

    public T[] Result { get; protected set; }

    #region Implementation of IEnumerator

    public bool MoveNext()
    {
        return !Complete;
    }

    public void Reset()
    {

    }

    public object Current => null;

    #endregion
}