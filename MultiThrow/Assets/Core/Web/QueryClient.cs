﻿using System;
using System.Net.Http;
using System.Text;
using UnityEngine;
using Object = System.Object;

public class QueryClient
{
    public string BaseAddress { get; set; }

    public Tuple<String, String> Token { get; set; }

    private readonly string _Query;

    private readonly URLHeader[] _Headers;

    public Boolean RawResult { get; set; } = false;

    private static HttpClient _HttpClient;

    public QueryClient(String query, params URLHeader[] headers)
    {
        _Query = query;
        _Headers = headers;

        _HttpClient = new HttpClient();
    }

    public Exception InnerException { get; private set; }

    public T PostResponse<T>(String contentData) where T : class
    {
        return PostResponse(contentData, typeof(T)) as T;
    }

    public Object PostResponse(String contentData, Type type)
    {
        //http://zetcode.com/csharp/httpclient/

        try
        {
            StringBuilder allHeaders = new StringBuilder();
            for (int i = 0; i < _Headers.Length; i++)
            {
                URLHeader header = _Headers[i];
                string marker = i == 0 ? "?" : "&";
                allHeaders.Append($"{marker}{header.Key}={header.Value}");
            }

            if (!string.IsNullOrEmpty(Token.Item1) && !string.IsNullOrEmpty(Token.Item2))
            {
                _HttpClient.DefaultRequestHeaders.Add(Token.Item1, Token.Item2);
            }

            StringContent data = new StringContent(contentData, Encoding.UTF8, "application/json");

            string result = _HttpClient.PostAsync($"{BaseAddress}{_Query}{allHeaders}", data).Result.Content.ReadAsStringAsync().Result;

            return JsonUtility.FromJson(result, type);
        }
        catch (Exception e)
        {
            InnerException = e;
            return null;
        }
    }

    public T PutResponse<T>(String contentData) where T : class
    {
        return PutResponse(contentData, typeof(T)) as T;
    }

    public Object PutResponse(String contentData, Type type)
    {
        try
        {
            StringBuilder allHeaders = new StringBuilder();
            for (int i = 0; i < _Headers.Length; i++)
            {
                URLHeader header = _Headers[i];
                string marker = i == 0 ? "?" : "&";
                allHeaders.Append($"{marker}{header.Key}={header.Value}");
            }

            if (!string.IsNullOrEmpty(Token.Item1) && !string.IsNullOrEmpty(Token.Item2))
            {
                _HttpClient.DefaultRequestHeaders.Add(Token.Item1, Token.Item2);
            }

            StringContent data = new StringContent(contentData, Encoding.UTF8, "application/json");

            string result = _HttpClient.PutAsync($"{BaseAddress}{_Query}{allHeaders}", data).Result.Content.ReadAsStringAsync().Result;

            return JsonUtility.FromJson(result, type);
        }
        catch (Exception e)
        {
            InnerException = e;
            return null;
        }
    }

    public T GetResponse<T>() where T : class
    {
        return GetResponse(typeof(T)) as T;
    }

    public Object GetResponse(Type type)
    {
        string rawResponse = GetRawResponse();
        return JsonUtility.FromJson(rawResponse, type);
    }

    public T[] GetArrayResponse<T>() where T : class
    {
        string rawResponse = GetRawResponse();
        return JsonArray.FromJson<T>(rawResponse);
    }

    public String GetRawResponse()
    {
        try
        {
            StringBuilder allHeaders = new StringBuilder();
            for (int i = 0; i < _Headers.Length; i++)
            {
                URLHeader header = _Headers[i];
                string marker = i == 0 ? "?" : "&";
                allHeaders.Append($"{marker}{header.Key}={header.Value}");
            }

            if (!string.IsNullOrEmpty(Token.Item1) && !string.IsNullOrEmpty(Token.Item2))
            {
                _HttpClient.DefaultRequestHeaders.Add(Token.Item1, Token.Item2);
            }

            string result = _HttpClient.GetStringAsync($"{BaseAddress}{_Query}{allHeaders}").Result;

            return result;
        }
        catch (Exception e)
        {
            InnerException = e;
            return null;
        }
    }
}
