﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class DatabaseRequestAsync<T> : IEnumerator where T : class
{
    protected QueryClient Client { get; private set; }

    protected DatabaseRequestAsync(String query, params URLHeader[] headers)
    {
        List<URLHeader> lheaders = headers.ToList();
        lheaders.Add(new URLHeader("content-type", "application/json"));
        lheaders.Add(new URLHeader("cache-control", "no-cache"));

        Client = new QueryClient(query, lheaders.ToArray())
        {
            Token = new Tuple<string, string>("x-apikey", DatabaseRequest.APIKEY),
            BaseAddress = DatabaseRequest.BaseAddress
        };
    }

    public Boolean Complete { get; protected set; }

    public Boolean Success { get; protected set; }

    public T Result { get; protected set; }

    #region Implementation of IEnumerator

    public bool MoveNext()
    {
        return !Complete;
    }

    public void Reset()
    {

    }

    public object Current => null;

    #endregion
}

public static class DatabaseRequest
{
    public static String BaseAddress;
    public static String APIKEY;
}