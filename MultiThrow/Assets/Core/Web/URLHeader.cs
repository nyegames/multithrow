﻿using System;

[Serializable]
public struct URLHeader
{
    public String Key;

    public String Value;

    public URLHeader(String key, System.Object value)
    {
        Key = key;
        Value = value.ToString();
    }

    #region Overrides of ValueType

    public override string ToString()
    {
        return $"{Key} : {Value}";
    }

    #endregion
}
