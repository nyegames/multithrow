﻿using System;
using UnityEngine;

namespace Glide
{
    public class Lerpers
    {
        public class Vector2Lerper : MemberLerper
        {
            Vector2 from, to, range;

            public override void Initialize(object fromValue, object toValue, Behavior behavior)
            {
                from = (Vector2)fromValue;
                to = (Vector2)(toValue);
                range = to - from;
            }

            public override object Interpolate(float t, object current, Behavior behavior)
            {
                Vector2 value = from + range * t;

                System.Type type = current.GetType();
                return Convert.ChangeType(value, type);
            }
        }

        public class Vector3Lerper : MemberLerper
        {
            Vector3 from, to, range;

            public override void Initialize(object fromValue, object toValue, Behavior behavior)
            {
                from = (Vector3)fromValue;
                to = (Vector3)(toValue);
                range = to - from;
            }

            public override object Interpolate(float t, object current, Behavior behavior)
            {
                Vector3 value = from + range * t;

                System.Type type = current.GetType();
                return Convert.ChangeType(value, type);
            }
        }

        public class ColorLerper : MemberLerper
        {
            Color from, to, range;

            public override void Initialize(object fromValue, object toValue, Behavior behavior)
            {
                from = (Color)fromValue;
                to = (Color)(toValue);
                range = to - from;
            }

            public override object Interpolate(float t, object current, Behavior behavior)
            {
                Color value = from + range * t;

                System.Type type = current.GetType();
                return Convert.ChangeType(value, type);
            }
        }
    }
}
